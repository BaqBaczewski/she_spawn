package com.shespawn.she_spawn;


import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;


public class SheSpawnApplication {

	/** The token is generated automatically when creating an account on Netlify */
	private static final String netlifyToken = "Iy-FfBtYHsv-z3_41gaQ7BTpgzpT4oFx4WWffNjFzbQ";

	/** HttpClient can be used to send requests and retrieve their responses */
	private static final HttpClient client = HttpClient.newHttpClient();

	public static void main(String[] args) throws Exception {

		createSite();
		createSiteDeploy("d8246a79-997f-4058-8f78-e11cf3e84f67");
		uploadDeployFile("60409779a906729c3cda2478", "src/main/resources/templates/index.html");
		uploadDeployFile("60409779a906729c3cda2478", "src/main/resources/templates/gallery.html");

		listSites();
	}

	/**
	 *
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void createSite() throws IOException, InterruptedException {

		HttpRequest httpRequestToCreate = HttpRequest.newBuilder()
				.uri(URI.create("https://api.netlify.com/api/v1/sites"))
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.POST(HttpRequest.BodyPublishers.noBody())
				.build();

		HttpResponse<String> responseToCreate = client.send(httpRequestToCreate,
				HttpResponse.BodyHandlers.ofString());

		System.out.println(responseToCreate.statusCode());
		System.out.println(responseToCreate.body());

	}

	/**
	 *
	 * @param site_id
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void createSiteDeploy(String site_id) throws IOException, InterruptedException {

		String json = "{\n" +
				"    \"files\":{\n" +
				"       \"index.html\":\"0620e80ab5dd35927b2579895c730f66da83a0f9\",\n" +
				"       \"gallery.html\": \"8d059b58f1ff0d0fde231e7e3af34b78447770e1\"\n" +
				"    }\n" +
				"}";


		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create("https://api.netlify.com/api/v1/sites/" + site_id + "/deploys"))
				.header("Content-Type", "application/json")
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.POST(HttpRequest.BodyPublishers.ofString(json))
				.build();

		HttpResponse<String> response = client.send(request,
				HttpResponse.BodyHandlers.ofString());


		System.out.println(response.statusCode());
		System.out.println(response.body());
	}

	/**
	 *
	 * @param deploy_id
	 * @param path
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void uploadDeployFile(String deploy_id, String path) throws IOException, InterruptedException {

		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create("https://api.netlify.com/api/v1/deploys/" + deploy_id + "/files/" + path))
				.header("Content-Type", "application/octet-stream")
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.PUT(HttpRequest.BodyPublishers.ofFile(Path.of(path)))
				.build();

		HttpResponse<String> response = client.send(request,
				HttpResponse.BodyHandlers.ofString());

		System.out.println(response.statusCode());
		System.out.println(response.body());

	}

	/**
	 *
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void listSites() throws IOException, InterruptedException {

		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create("https://api.netlify.com/api/v1/sites"))
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.build();

		HttpResponse<String> response = client.send(request,
				HttpResponse.BodyHandlers.ofString());

//        System.out.println(response.statusCode());
//        System.out.println(response.body());

	}

	/**
	 *
	 * @param site_id
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void getSite(String site_id) throws IOException, InterruptedException {

		HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create("https://api.netlify.com/api/v1/sites" + site_id))
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.build();

		HttpResponse<String> response = client.send(request,
				HttpResponse.BodyHandlers.ofString());

//        System.out.println(response.statusCode());
//        System.out.println(response.body());
	}
}


