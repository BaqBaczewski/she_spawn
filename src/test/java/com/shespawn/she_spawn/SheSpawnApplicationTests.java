package com.shespawn.she_spawn;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;


class SheSpawnApplicationTests {

	/** Token Netlify */
	String netlifyToken = "Iy-FfBtYHsv-z3_41gaQ7BTpgzpT4oFx4WWffNjFzbQ";
	String site_id = "d8246a79-997f-4058-8f78-e11cf3e84f67";
	String deploy_id = "60409779a906729c3cda2478";

	/**
	 * Check access token Netlify.com
	 * HTTP 200 OK when current token
	 */
	@Test
	void checkAccessToken() throws URISyntaxException, IOException, InterruptedException {
		HttpRequest request = HttpRequest.newBuilder()
				.uri(new URI("https://api.netlify.com/api/v1/user"))
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.build();

		HttpResponse<String> response = HttpClient.newBuilder()
				.build()
				.send(request, HttpResponse.BodyHandlers.ofString());

		Assertions.assertEquals(response.statusCode(), HttpURLConnection.HTTP_OK);
		Assertions.assertEquals(request.uri().toString(), "https://api.netlify.com/api/v1/user");
		Assertions.assertNotNull(response.body());

	}



	/**
	 * create a site
	 * HTTP 201 The request has succeeded and a new resource has been created as a result
	 */
	@Test
	void createSite() throws URISyntaxException, IOException, InterruptedException {

		HttpRequest request = HttpRequest.newBuilder()
				.uri(new URI("https://api.netlify.com/api/v1/sites/"))
				.version(HttpClient.Version.HTTP_2)
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.POST(HttpRequest.BodyPublishers.noBody())
				.build();

		HttpResponse<String> response = HttpClient.newBuilder()
				.build()
				.send(request, HttpResponse.BodyHandlers.ofString());

		Assertions.assertEquals(request.uri().toString(), "https://api.netlify.com/api/v1/sites/");
		Assertions.assertEquals(response.statusCode(), HttpURLConnection.HTTP_CREATED);
		Assertions.assertEquals(response.version(), HttpClient.Version.HTTP_2);
		Assertions.assertNotNull(response.body());
	}

	/**
	 * HTTP 200 OK send deploy on site
	 * check content length for this request body
	 */
	@Test
	void createSiteDeploy() throws URISyntaxException, IOException, InterruptedException {

		String json = "{\n" +
				"    \"files\":{\n" +
				"       \"index.html\":\"0620e80ab5dd35927b2579895c730f66da83a0f9\",\n" +
				"       \"gallery.html\": \"8d059b58f1ff0d0fde231e7e3af34b78447770e1\"\n" +
				"    }\n" +
				"}";


		HttpRequest request = HttpRequest.newBuilder()
				.uri(new URI("https://api.netlify.com/api/v1/sites/" + site_id + "/deploys"))
				.version(HttpClient.Version.HTTP_2)
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers.ofString(json))
				.build();

		HttpResponse<String> response = HttpClient.newBuilder()
				.build()
				.send(request, HttpResponse.BodyHandlers.ofString());

		Assertions.assertEquals(request.bodyPublisher().get().contentLength(), json.getBytes().length);
		Assertions.assertEquals(response.version(), HttpClient.Version.HTTP_2);
		Assertions.assertEquals(response.statusCode(), HttpURLConnection.HTTP_OK);
		assertThat(request.uri().toString(), containsString("https://api.netlify.com/api/v1/sites/"));
		Assertions.assertNotNull(response.body());
	}

	/**
	 * HTTP 200 OK upload file to deploy
	 */
	@Test
	void uploadDeployFile() throws URISyntaxException, IOException, InterruptedException {

		String path = "src/main/resources/templates/index.html";

		HttpRequest request = HttpRequest.newBuilder()
				.uri(new URI("https://api.netlify.com/api/v1/deploys/" + deploy_id + "/files/" + path))
				.version(HttpClient.Version.HTTP_2)
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.header("Content-Type", "application/octet-stream")
				.PUT(HttpRequest.BodyPublishers.ofFile(Path.of(path)))
				.build();

		HttpResponse<String> response = HttpClient.newBuilder()
				.build()
				.send(request, HttpResponse.BodyHandlers.ofString());

		Assertions.assertEquals(response.statusCode(), HttpURLConnection.HTTP_OK);
		Assertions.assertEquals(response.version(), HttpClient.Version.HTTP_2);
		Assertions.assertNotNull(response.body());
		assertThat(request.uri().toString(), containsString("https://api.netlify.com/api/v1/deploys/"));
	}


	/**
	 * get list sites from Netlify.com
	 * HTTP 200 OK - The resource has been fetched and is transmitted in the message body.
	 * 401 - unauthorized, missed token
	 */
	@Test
	void listSitesUnAuthorized() throws URISyntaxException, IOException, InterruptedException {
		HttpRequest request = HttpRequest.newBuilder()
				.uri(new URI("https://api.netlify.com/api/v1/sites"))
				.version(HttpClient.Version.HTTP_2)
				.build();

		HttpResponse<String> response = HttpClient.newBuilder()
				.build()
				.send(request, HttpResponse.BodyHandlers.ofString());

		Assertions.assertEquals(response.statusCode(), HttpURLConnection.HTTP_UNAUTHORIZED);
		Assertions.assertEquals(response.version(), HttpClient.Version.HTTP_2);
		assertThat(request.uri().toString(), containsString("https://api.netlify.com/api/v1/sites"));
		Assertions.assertNotNull(response.body());
	}

	/**
	 * get site by site_id
	 * check if this request was sent to the required url address
	 */
	@Test
	void getSite() throws URISyntaxException, IOException, InterruptedException {

		HttpRequest request = HttpRequest.newBuilder()
				.uri(new URI("https://api.netlify.com/api/v1/sites/" + site_id))
				.version(HttpClient.Version.HTTP_2)
				.header("Authorization", "Bearer" + " " + netlifyToken)
				.build();

		HttpResponse<String> response = HttpClient.newBuilder()
				.build()
				.send(request, HttpResponse.BodyHandlers.ofString());

		Assertions.assertEquals(response.statusCode(), HttpURLConnection.HTTP_OK);
		Assertions.assertEquals(response.version(), HttpClient.Version.HTTP_2);
		assertThat(request.uri().toString(), containsString("https://api.netlify.com/api/v1/sites/"));
		Assertions.assertNotNull(response.body());
	}
}

