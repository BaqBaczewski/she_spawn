package com.shespawn.she_spawn

import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.nio.file.Path
import spock.lang.Shared
import spock.lang.Specification


class SheSpawnApplicationTest extends Specification {

    def "two plus two should equal four"() {
        given:
        int left = 2
        int right = 2

        when:
        int result = left + right

        then:
        result == 4
    }

//    def "Main"() {
//    }
//
//    def "CreateSite"() {
//    }
//
//    def "CreateSiteDeploy"() {
//    }
//
//    def "UploadDeployFile"() {
//    }
//
//    def "ListSites"() {
//    }
//
//    def "GetSite"() {
//    }
}
